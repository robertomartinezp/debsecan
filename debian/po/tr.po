# Turkish translation of debsecan debconf template.
# Copyright (C) 2008 Mert Dirik
# This file is distributed under the same license as the debsecan package.
# Mert Dirik <mertdirik@gmail.com>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: debsecan 0.4.10+nmu1\n"
"Report-Msgid-Bugs-To: debsecan@packages.debian.org\n"
"POT-Creation-Date: 2008-10-12 14:38+0200\n"
"PO-Revision-Date: 2008-09-28 19:22+0200\n"
"Last-Translator: Mert Dirik <mertdirik@gmail.com>\n"
"Language-Team: Debian L10n Turkish <debian-l10n-turkish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../debsecan.templates:1001
msgid "Do you want debsecan to send daily reports?"
msgstr "debsecan'ın günlük raporlar göndermesini ister misiniz?"

#. Type: boolean
#. Description
#: ../debsecan.templates:1001
msgid ""
"debsecan can check the security status of the host once per day, and notify "
"you of any changes by email."
msgstr ""
"debsecan her gün makinenin güvenlik durumunu denetleyip değişiklikleri size "
"e-posta ile bildirebilir."

#. Type: boolean
#. Description
#: ../debsecan.templates:1001
msgid ""
"If you choose this option, debsecan will download a small file once a day.  "
"Your package list will not be transmitted to the server."
msgstr ""
"Bu seçeneği tercih ederseniz debsecan her gün küçük bir dosya indirecek.  "
"Paket listeniz sunucuya aktarılmayacak."

#. Type: string
#. Description
#: ../debsecan.templates:2001
msgid "Email address to which daily reports should be sent:"
msgstr "Günlük raporların gönderileceği e-posta adresi:"

#. Type: select
#. Description
#: ../debsecan.templates:3001
msgid "Main suite from which packages are installed:"
msgstr "Paketlerin kurulu olduğu ana süit:"

#. Type: select
#. Description
#: ../debsecan.templates:3001
msgid ""
"To present more useful data, debsecan needs to know the Debian release from "
"which you usually install packages."
msgstr ""
"Daha yararlı veriler sunabilmek için debsecan'ın, sisteminize paketlerini "
"kurduğunuz Debian sürümünü bilmesi gereklidir."

#. Type: select
#. Description
#: ../debsecan.templates:3001
msgid ""
"If you specify \"GENERIC\" (the default), only basic debsecan functionality "
"is available.  If you specify the suite matching your sources.list "
"configuration, information about fixed and obsolete packages will be "
"included in email reports."
msgstr ""
"Eğer \"GENERIC\"i seçerseniz (öntanımlı), yalnızca temel debsecan "
"işlevlerini kullanabilirsiniz.  Eğer sources.list yapılandırmanıza en uygun "
"süiti seçerseniz e-posta raporlarına açıkları giderilmiş paketler ve eski "
"paketler hakkındaki bilgiler de eklenir."

#. Type: string
#. Description
#: ../debsecan.templates:4001
msgid "URL of vulnerability information:"
msgstr "Güvenlik açığı bilgilerinin URL'si:"

#. Type: string
#. Description
#: ../debsecan.templates:4001
msgid ""
"debsecan fetches vulnerability information from the network. If your system "
"is not connected to the Internet, you can enter the URL of a local mirror "
"here.  If you leave this option empty, the built-in default URL is used."
msgstr ""
"debsecan güvenlik açıkları bilgisini ağdan edinir. Eğer sisteminiz "
"Internet'e bağlı değilse, yerel yansının adresini buraya girebilirsiniz. Bu "
"seçeneği boş bırakırsanız öntanımlı yerleşik URL kullanılır."
